using System.Runtime.Caching;


    public class PasswordManager
    {
        private static readonly ObjectCache _cache = MemoryCache.Default;
        private static readonly CacheItemPolicy _cachePolicy = new CacheItemPolicy();

        public static List<Password> GetPasswords()
        {
            var passwords = _cache["passwords"] as List<Password>;
            if (passwords == null)
            {
                passwords = new List<Password>();
                _cache.Set("passwords", passwords, _cachePolicy);
            }

            return passwords;
        }

        public static void AddPassword(Password newPassword)
        {
            var passwords = GetPasswords();
            newPassword.Id = passwords.Count > 0 ? passwords.Max(p => p.Id) + 1 : 1;
            passwords.Add(newPassword);
        }

        public static Password GetPassword(int id)
        {
            var passwords = GetPasswords();
            return passwords.FirstOrDefault(p => p.Id == id);
        }

        public static Password GetDecryptedPassword(int id)
        {
            var password = GetPassword(id);
            if (password != null)
            {
                password.EncryptedPassword = DecryptPassword(password.EncryptedPassword);
            }
            return password;
        }

        public static void UpdatePassword(Password updatedPassword)
        {
            var passwords = GetPasswords();
            var password = passwords.FirstOrDefault(p => p.Id == updatedPassword.Id);
            if (password != null)
            {
                password.Category = updatedPassword.Category;
                password.App = updatedPassword.App;
                password.UserName = updatedPassword.UserName;
                password.EncryptedPassword = updatedPassword.EncryptedPassword;
            }
        }

        public static void DeletePassword(int id)
        {
            var passwords = GetPasswords();
            var password = passwords.FirstOrDefault(p => p.Id == id);
            if (password != null)
            {
                passwords.Remove(password);
            }
        }

        private static string DecryptPassword(string encryptedPassword)
        {
            byte[] data = Convert.FromBase64String(encryptedPassword);
            return System.Text.Encoding.ASCII.GetString(data);
        }
    }

