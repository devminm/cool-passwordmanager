﻿// Online C# Editor for free
// Write, Edit and Run your C# code using C# Online Compiler

using System;

public class Program
{
    public static void Main(string[] args)
    {
       
     // Add a password
        Password newPassword = new Password
        {
            Id = 1,
            Category = "work",
            App = "outlook",
            UserName = "testuser@mytest.com",
            EncryptedPassword = "TXlQYXNzd29yZEAxMjM="
        };
        PasswordManager.AddPassword(newPassword);

        // Get all passwords
        var allPasswords = PasswordManager.GetPasswords();
        Console.WriteLine("All passwords:");
        foreach (var password in allPasswords)
        {
            Console.WriteLine($"Id: {password.Id}, Category: {password.Category}, App: {password.App}, UserName: {password.UserName}, EncryptedPassword: {password.EncryptedPassword}");
        }

        // Get a password by id
        int passwordId = 1;
        var passwordById = PasswordManager.GetPassword(passwordId);
        Console.WriteLine($"Password with id {passwordId}:");
        if (passwordById != null)
        {
            Console.WriteLine($"Id: {passwordById.Id}, Category: {passwordById.Category}, App: {passwordById.App}, UserName: {passwordById.UserName}, EncryptedPassword: {passwordById.EncryptedPassword}");
        }
        else
        {
            Console.WriteLine("Password not found");
        }

        // Get a password with decrypted password
        var passwordWithDecrypted = PasswordManager.GetDecryptedPassword(passwordId);
        Console.WriteLine($"Password with id {passwordId} and decrypted password:");
        if (passwordWithDecrypted != null)
        {
            Console.WriteLine($"Id: {passwordWithDecrypted.Id}, Category: {passwordWithDecrypted.Category}, App: {passwordWithDecrypted.App}, UserName: {passwordWithDecrypted.UserName}, Password: {passwordWithDecrypted.EncryptedPassword}");
        }
        else
        {
            Console.WriteLine("Password not found");
        }

        // Update a password
        newPassword.UserName = "updateduser@mytest.com";
        PasswordManager.UpdatePassword(newPassword);
        var updatedPassword = PasswordManager.GetPassword(passwordId);
        Console.WriteLine($"Updated password with id {passwordId}:");
        if (updatedPassword != null)
        {
            Console.WriteLine($"Id: {updatedPassword.Id}, Category: {updatedPassword.Category}, App: {updatedPassword.App}, UserName: {updatedPassword.UserName}, EncryptedPassword: {updatedPassword.EncryptedPassword}");
        }
        else
        {
            Console.WriteLine("Password not found");
        }

        // Delete a password
        PasswordManager.DeletePassword(passwordId);
        var deletedPassword = PasswordManager.GetPassword(passwordId);
        Console.WriteLine($"Deleted password with id {passwordId}:");
        if (deletedPassword != null)
        {
            Console.WriteLine($"Id: {deletedPassword.Id}, Category: {deletedPassword.Category}, App: {deletedPassword.App}, UserName: {deletedPassword.UserName}, EncryptedPassword: {deletedPassword.EncryptedPassword}");
        }
        else
        {
            Console.WriteLine("Password not found");
        }

    }
}


   